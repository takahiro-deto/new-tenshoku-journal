@extends('layout.layout')

{{-- l-wrapper's id --}}
@section('page-id','p-detail')

@section('content')
<div class="l-main">
  <div class="l-inner">
    <div class="ui breadcrumb">
      <div class="l-inner">
        <div class="c-breadcrumb">
          <a class="section" href="#">HOME</a>
          <i class="right angle icon divider"></i>
          <a class="section" href="{{ action('RankingController@index') }}">転職サイト・Agent</a>
          <i class="right angle icon divider"></i>

          @if($vars['contents']->service_type == 'site')
          <a class="section" href="{{ action('RankingController@index','site') }}">転職サイト総合ランキング</a>
          @elseif($vars['contents']->service_type == 'agent')
          <a class="section" href="{{ action('RankingController@index','agent') }}">転職エージェント徹底比較ランキング</a>
          @elseif($vars['contents']->service_type == 'haken')
          <a class="section" href="{{ action('RankingController@index','haken') }}">派遣サイトおすすめランキング</a>
          @elseif($vars['contents']->service_type == 'woman')
          <a class="section" href="{{ action('RankingController@index','woman') }}">女性向け転職サイト・転職エージェント総合ランキング</a>
          @elseif($vars['contents']->service_type == 'itweb')
          <a class="section" href="{{ action('RankingController@index','itweb') }}">ITWeb系転職サイト・エージェントランキング</a>
          @endif

          <i class="right angle icon divider"></i>
          <span>{{ $vars['contents']->service_jp_name }}の評判・口コミ</span></div>
      </div>
    </div>
  </div>
  <div class="l-inner l-row">
    <main class="l-col-lg-8">
      @include('inc.detail')
    </main>
    <div class="l-col-lg-4">
      <div class="l-aside">

        @if($vars['contents']->service_type == 'site')
          @include('inc.sidebar.site')
        @elseif($vars['contents']->service_type == 'agent')
          @include('inc.sidebar.agent')
        @elseif($vars['contents']->service_type == 'itweb')
          @include('inc.sidebar.itweb')
        @elseif($vars['contents']->service_type == 'haken')
          @include('inc.sidebar.haken')
        @elseif($vars['contents']->service_type == 'woman')
          @include('inc.sidebar.woman')
        @endif

        <aside class="l-aside_rg_2">
          <div class="c-knowhow">
            <h2>タイプ別転職成功ノウハウ</h2>
            <ul class="c-knowhow__list">

              @forelse($vars['pages'] as $page)
              <li class="c-knowhow__item">
                <a class="c-knowhow__link" href="{{ make_relative_path($page->guid) }}">
                  <img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/feature-{{ $page->ID }}.jpg" width="70" height="55" alt="{{ $page->post_title }}">
                  <p class="c-knowhow__lead">{{ $page->post_title }}</p>
                </a>
              </li>
              @empty
              <li></li>
              @endforelse

            </ul>
          </div>
          <div class="c-knowhow">
            <h2>転職を考えたら</h2>
            <ul class="c-knowhow__list">

              @forelse($vars['posts'] as $post)
              <li class="c-knowhow__item">
                <a class="c-knowhow__link" href="{{ make_relative_path($post->link) }}">
                  <img class="c-knowhow__thumb" src="{{ make_relative_path($post->thumb) }}" width="70" height="55" alt="{{ $post->title }}">
                  <p class="c-knowhow__lead">{{ $post->title }}</p>
                </a>
              </li>
              @empty
              <li></li>
              @endforelse

            </ul>
          </div>
        </aside>
      </div>
    </div>
  </div>
</div>
@endsection
