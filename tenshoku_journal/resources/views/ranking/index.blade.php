@extends('layout.layout')

{{-- l-wrapper's id --}}
@section('page-id','p-ranking')

@section('content')
<div class="l-hero">
  <div class="c-hero c-hero__{{ $vars['service_type'] }}">
    <div class="l-inner">
      <div class="ui breadcrumb">
        <div class="c-breadcrumb">
          <a class="section" href="/">HOME</a>
          <i class="right angle icon divider"></i>
          <a class="section" href="{{ action('RankingController@index') }}">転職サイト・Agent</a>
          <i class="right angle icon divider"></i>
          <div class="section">{{ $vars['breadcrumbText'] }}</div>
        </div>
      </div>
      <h1 class="c-hero__title">{{ $vars['h1Text'] }}</h1>
    </div>
  </div>
</div>
<div class="l-main">
  <div class="l-inner l-row">
    <main class="l-col-lg-8">
      <div class="l-main_lf_1">
        <div class="c-headline">
          {!! nl2br($vars['headline']) !!}
        </div>
      </div>
      <div class="l-main_lf_2">

        @foreach($vars['rankings'] as $ranking)

        <div class="c-ranking">
          <h2 class="c-ranking__title"><span>第{{ $ranking->rank }}位</span>{{ $ranking->service_jp_name }}</h2>
          <div class="ui items c-ranking__summary">
            <div class="item">
              <a href="{{ action('RankingController@show', [$ranking->service_type, $ranking->service_id]) }}" class="ui small image">
                <img class="c-ranking__thumb" src="http://data.tenshoku-journal.com/{{ $ranking->thumbnail_path }}" alt="{{ $ranking->service_jp_name }}">
              </a>
              <div class="content">
                <div class="description">
                  <p>{{ $ranking->summary }}</p>
                </div>
              </div>
            </div>
          </div>
          <div class="c-ranking__description">
            <div class="c-ranking__section">
              <span class="c-ranking__catch">ここがポイント<i class="pointing up icon"></i></span>
              <ul class="c-ranking__points">

                @if(isset($ranking->positive_point_1))
                <li class="c-ranking__point"><i class="checkmark icon c-ranking__pointIcon"></i>{{ $ranking->positive_point_1 }}</li>
                @endif

                @if(isset($ranking->positive_point_2))
                <li class="c-ranking__point"><i class="checkmark icon c-ranking__pointIcon"></i>{{ $ranking->positive_point_2 }}</li>
                @endif

                @if(isset($ranking->positive_point_3))
                <li class="c-ranking__point"><i class="checkmark icon c-ranking__pointIcon"></i>{{ $ranking->positive_point_3 }}</li>
                @endif

              </ul>
            </div>

            {{--
            <div class="c-ranking__section"><span class="c-ranking__catch">利用者の声<i class="talk outline icon"></i></span>
              <div class="ui comments c-voices">

                @foreach($vars['reputations'][$loop->index] as $reputation)
                <div class="ui raised segment">
                  <div class="comment c-voice">
                    <div class="c-voice__author">
                      <div class="avatar c-voice__avatar">
                        <span class="{{ $reputation->avatar_type }}"></span>
                      </div>
                      <div class="content c-voice__meta">
                        <span class="author">
                          {{ $reputation->age }} / {{ $reputation->gender }} / {{ $reputation->job }}
                        </span>
                        <div class="metadata">
                          <div class="ui star rating disabled" data-rating="{{ $reputation->rating }}" data-max-rating="5"></div>
                        </div>
                      </div>
                    </div>
                    <div class="content c-voice__comment">
                      <div class="text">
                        {{ mb_substr($reputation->comment,0,199) }}...
                      </div>
                    </div>
                  </div>
                </div>
                @endforeach

              </div>
            </div>
            --}}

            @if($ranking->crawling_flag === 1)
            <div class="c-double__buttons">
              <div class="ui orange button c-button">
                {{--<a href="{{ action('RankingController@show', [$ranking->service_type, $ranking->service_id]) }}">クチコミ・詳細を見る</a>--}}
                <a href="{{ action('RankingController@show', [$ranking->service_type, $ranking->service_id]) }}">詳細を見る</a>
              </div>
              <div class="ui teal button c-button">
                <a href="{{ action('SearchController@getIndexByServiceId', $ranking->service_id )}}">{{ $ranking->service_jp_name }}の求人をみる</a>
              </div>
            </div>
            @else
            <div class="c-single__button">
              <div class="ui orange button c-button">
                {{--<a href="{{ action('RankingController@show', [$ranking->service_type, $ranking->service_id]) }}">クチコミ・詳細を見る</a>--}}
                <a href="{{ action('RankingController@show', [$ranking->service_type, $ranking->service_id]) }}">詳細を見る</a>
              </div>
            </div>
            @endif

          </div>
        </div>
        @endforeach

      </div>
    </main>
    <div class="l-col-lg-4">
      <div class="l-aside">

        @if($vars['service_type'] == 'site')
          @include('inc.sidebar.site')
        @elseif($vars['service_type'] == 'agent')
          @include('inc.sidebar.agent')
        @elseif($vars['service_type'] == 'itweb')
          @include('inc.sidebar.itweb')
        @elseif($vars['service_type'] == 'haken')
          @include('inc.sidebar.haken')
        @elseif($vars['service_type'] == 'woman')
          @include('inc.sidebar.woman')
        @endif

        <aside class="l-aside_rg_2">
          <div class="c-knowhow">
            <h2>タイプ別転職成功ノウハウ</h2>
            <ul class="c-knowhow__list">

              @forelse($vars['pages'] as $page)
              <li class="c-knowhow__item">
                <a class="c-knowhow__link" href="{{ make_relative_path($page->guid) }}">
                  <img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/feature-{{ $page->ID }}.jpg" width="70" height="55" alt="{{ $page->post_title }}">
                  <p class="c-knowhow__lead">{{ $page->post_title }}</p>
                </a>
              </li>
              @empty
              <li></li>
              @endforelse

            </ul>
          </div>
          <div class="c-knowhow">
            <h2>転職を考えたら</h2>
            <ul class="c-knowhow__list">

              @forelse($vars['posts'] as $post)
              <li class="c-knowhow__item">
                <a class="c-knowhow__link" href="{{ make_relative_path($post->link) }}">
                  <img class="c-knowhow__thumb" src="{{ make_relative_path($post->thumb) }}" width="70" height="55" alt="{{ $post->title }}">
                  <p class="c-knowhow__lead">{{ $post->title }}</p>
                </a>
              </li>
              @empty
              <li></li>
              @endforelse

            </ul>
          </div>
        </aside>
      </div>
    </div>
  </div>
</div>
@endsection
