@if(isset($headers))
<meta name="Keywords" content="{{ $headers['metaKeywords'] }}">
<meta name="Description" content="{{ $headers['metaDescription'] }}">
<title>転職ジャーナル | {{ $headers['title'] }}</title>
@else
<meta name="Keywords" content="転職、求人、転職サイト、転職エージェント、口コミ、評判">
<meta name="Description" content="転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。">
<title>転職ジャーナル | 転職を考える全ての人のためのサイト</title>
@endif
@if (isset($headers['canonical_path']))
<link rel="canonical" href="{{ get_canonical_url($headers['canonical_path']) }}" />
@endif