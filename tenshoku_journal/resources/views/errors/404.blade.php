@extends('layout.layout')

{{-- l-wrapper's id --}}
@section('page-id','p-top')

@section('content')
<div class="l-main">
  <div class="l-inner l-row">
    <div class="l-col-lg-8" style="margin: 60px auto;">
      <h1 style="color: #666;">お探しのページは見つかりません</h1>
      <p>申し訳ございませんがあなたがアクセスしようとしたページは削除されたか、URLが変更されています。<br/>前のページに戻られるか、求人をお探しの場合は下記よりもう一度検索してください。</p>
      <div class="c-search" style="margin-top:100px;">
        <div class="l-search_by l-search_by_work">
          <h3>領域・職種からさがす</h3>
          <div class="c-search__list">
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__itweb" href="/search/job/itweb"><span class="c-search__meta u-p2em">IT / Web</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__creative" href="/search/job/creative"><span class="c-search__meta u-p2em">クリエイティブ</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__bkoffice" href="/search/job/backoffice"><span class="c-search__meta u-p05em">バックオフィス<br><small>一般/専門事務・総務・財務・経理・法務・人事など</small></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__sales" href="/search/job/sales"><span class="c-search__meta u-p2em">営業</span></a></div>
            </div>
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__medical" href="/search/job/medical"><span class="c-search__meta u-p2em">医療</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__food" href="/search/job/food"><span class="c-search__meta u-p2em">飲食</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__logi" href="/search/job/logi_security"><span class="c-search__meta u-p1em">運輸・物流<br>警備・設備管理</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__care" href="/search/job/welfare"><span class="c-search__meta u-p2em">介護・福祉・保育</span></a></div>
            </div>
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__marke" href="/search/job/marketing"><span class="c-search__meta u-p05em">企画<br><small>経営・事業・商品企画<br>マーケティング、管理職</small></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__edu" href="/search/job/education"><span class="c-search__meta u-p2em">教育</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__finance" href="/search/job/finance"><span class="c-search__meta u-p2em">金融                        </span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__estate" href="/search/job/realestate"><span class="c-search__meta u-p1em">不動産・建築<br>設計・土木</span></a></div>
            </div>
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__service" href="/search/job/service"><span class="c-search__meta u-p2em">販売・サービス</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__prod" href="/search/job/manufacturering"><span class="c-search__meta u-p1em">製造・生産<br>開発・研究職</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__prof" href="/search/job/profession"><span class="c-search__meta u-p1em">専門職<br><small>コンサルタント・士業等</small></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__other" href="/search/job/other"><span class="c-search__meta u-p2em">その他</span></a></div>
            </div>
          </div>
        </div>
        <div class="l-search_by l-search_by_area">
          <h3>勤務地からさがす</h3>
          <div class="c-search__list">
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__hokkaido" href="/search/area/hokkaido"><span class="c-search__meta u-p2em">北海道</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__tohoku" href="/search/area/tohoku"><span class="c-search__meta u-p2em">東北</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__kanto" href="/search/area/kanto"><span class="c-search__meta u-p2em">関東</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__tokai" href="/search/area/tokai"><span class="c-search__meta u-p2em">東海</span></a></div>
            </div>
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__hokushin" href="/search/area/hokushinetsu"><span class="c-search__meta u-p2em">北信越</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__kansai" href="/search/area/kansai"><span class="c-search__meta u-p2em">関西</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__chushikoku" href="/search/area/chugokuShikoku"><span class="c-search__meta u-p2em">中国・四国</span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__kyushu" href="/search/area/kyushu"><span class="c-search__meta u-p2em">九州・沖縄</span></a></div>
            </div>
          </div>
        </div>
        <div class="l-search_by l-search_by_service">
          <h3>サービス名からさがす</h3>
          <div class="c-search__list">
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__next" href="/search/service/rikunabi_next"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__rag" href="/search/service/recruit_agent"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__wkpt" href="/search/service/workport"><span class="c-search__meta u-p2em"></span></a></div>
            </div>
            {{-- <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__baitoru" href="/search/service/baitoru_next"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__hatalike" href="/search/service/hatalike"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__mynabi" href="/search/service/mynabi_agent"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__next" href="/search/service/rikunabi_next"><span class="c-search__meta u-p2em"></span></a></div>
            </div>
            <div class="l-row l-md-row is-sp">
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__rag" href="/search/service/recruit_agent"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__tora" href="/search/service/torabayu"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__type" href="/search/service/at_type"><span class="c-search__meta u-p2em"></span></a></div>
              <div class="c-search__item l-col-lg-3 l-col-md-3 u-ml13"><a class="c-search__link c-search__wkpt" href="/search/service/workport"><span class="c-search__meta u-p2em"></span></a></div>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
