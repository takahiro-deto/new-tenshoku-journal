<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    @include('inc.head')    
    <link rel="shortcut icon" href="http://data.tenshoku-journal.com/icon.ico">
    <link rel="stylesheet" href="/knowhow/wp-content/themes/journal/dist/semantic/semantic.css">
    <link rel="stylesheet" href="/dist/css/style.css">
    <script type="text/javascript" src="/knowhow/wp-content/themes/journal/dist/js/jquery-3.2.0.min.js"></script>        
    <script type="text/javascript" src="/knowhow/wp-content/themes/journal/dist/semantic/semantic.js"></script>
    <script type="text/javascript" src="/dist/js/app.js"></script>
    @if(env('APP_URL') != 'http://localhost')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5X72D4N');</script>
    <!-- End Google Tag Manager -->
    @endif
  </head>
  <body>
    @if(env('APP_URL') != 'http://localhost')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5X72D4N"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @endif
    <div class="l-wrapper" id="@yield('page-id')">
    {{-- header template--}}
    @include('inc.header')

    {{-- main template--}}
    @yield('content')

    {{-- footer template--}}
    @include('inc.footer')
    </div>
  </body>
</html>
