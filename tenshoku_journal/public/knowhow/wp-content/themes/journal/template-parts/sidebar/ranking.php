<aside class="l-aside_rg_2">
  <div class="c-knowhow">
    <h2 style="font-weight: bold;">最新転職サイトランキング</h2>
    <ul class="c-knowhow__list">
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/site/rikunabi_next"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_next.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第１位　リクナビNEXT</p>
          </div></a></li>
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/site/at_type"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_type.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第２位　@type</p>
          </div></a></li>
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/site/baitoru_next"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_baitoru.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第３位　バイトルNEXT</p>
          </div></a></li>
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/site/hatalike"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_hatalike.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第４位　はたらいく</p>
          </div></a></li>
    </ul>
  </div>
</aside>
<aside class="l-aside_rg_2">
  <div class="c-knowhow">
    <h2 style="font-weight: bold;">最新転職エージェントランキング</h2>
    <ul class="c-knowhow__list">
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/agent/recruit_agent"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_rag.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第１位　リクルートエージェント</p>
          </div></a></li>
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/agent/doda"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_doda.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第２位　DODA</p>
          </div></a></li>
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/agent/workport"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_workport.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第３位　ワークポート</p>
          </div></a></li>
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/agent/type_agent"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_typeagent.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第４位　type転職エージェント</p>
          </div></a></li>
      <li class="c-knowhow__item"><a class="c-knowhow__link" href="/ranking/agent/mynabi_agent"><img class="c-knowhow__thumb" src="http://data.tenshoku-journal.com/tmb_mynabiagent.jpg">
          <div class="c-knowhow__lead">
            <p style="font-size:14px;">第５位　マイナビエージェント</p>
          </div></a></li>
    </ul>
  </div>
</aside>
