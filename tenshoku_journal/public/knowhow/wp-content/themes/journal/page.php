<?php get_header(); ?>
<div class="l-main" id="p-knowhow">
  <div class="l-inner l-row">
    <?php get_breadcrumb(); ?>
  </div>
  <div class="l-inner">
    <?php
      if( have_posts() ):
        while( have_posts() ): the_post();
    ?>
    <div class="ui divider u-dn"></div>
    <div class="c-articleHeader">
      <?php get_eyecatch(); ?>
      <div class="c-articleHeader__txtbox">
        <h1 class="c-articleHeader__title"><?php the_title(); ?></h1>
      </div>
      <p class="c-articleHeader__lead">
        <?php echo get_post_meta($post->ID, 'headline', true); ?>
      </p>
    </div>
    <?php
      endwhile;
      endif;
    ?>
    <div class="ui divider"></div>
  </div>
  <div class="l-inner l-row c-articleMain">
    <div class="l-col-lg-8">
      <div class="c-articleContent">
        <?php
          if( have_posts() ):
            while( have_posts() ): the_post();

            the_content();

            endwhile;
          endif;
        ?>
      </div>
    </div>
    <?php get_sidebar('page'); ?>
  </div>
</div>
<?php get_footer(); ?>
