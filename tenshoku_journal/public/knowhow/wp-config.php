<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'journal');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'journal');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'awE_M1Y@Xp>xUW(s');

$web_container_ip = getHostByName(getHostName());
$db_container_ip = substr($web_container_ip,0,-1) . '2';

/** MySQL のホスト名 */
define('DB_HOST', $db_container_ip);

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ibm+{,+/;}aYD!p^@_z|O,`~VkNG.jm}-.8d|N^RMgF]eS%|v4q;veaMa{(CfvSB');
define('SECURE_AUTH_KEY',  '1oVlymQUsEu9u*{C|n{_),0>&C]>7WE$l8lf0 HRuprRA{-ATB_Qx96(jjpucUJJ');
define('LOGGED_IN_KEY',    'f%2dDw/FJ[7aJ *C~BK6?;otaa./<.U0MDCEws-3Nxl/PJ=b~6YwAwc7HVT~hmXD');
define('NONCE_KEY',        '?>)r|w3TSoCX`k40?Y@^1U)r-jH:Ngxw@<+BIBIp R68[G%nkySZe9d`u9<,f0IT');
define('AUTH_SALT',        '+[:YYWY7`4!mj3B-.i=i),GI|N.2VBXRk1,Gr+ s-&PK]E9=-(nh|l+hT>|kr]{W');
define('SECURE_AUTH_SALT', 'c;/J84khan+/!eYkT%u<39|,v~N[D@s;>}UWF=1uN9KS4 #wS3+9~+CyN]CX-M/0');
define('LOGGED_IN_SALT',   '=!x-Up9HO(/Ib}Jq(AOUWW~q?`0ptVFEie;e/TVC?q_q)H1v[5bUHd=9|%V&f~}`');
define('NONCE_SALT',       '>mbHZ^?CVIzd|+P?,~3l2CiEf+5bV5WO@T<R%#+v-X]_pK4aq1K,GR6CbNoqBB]m');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

define('FS_METHOD', 'direct');
/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
