#!/usr/bin/bash
#PATH
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

GCS_SCRAPY_ROOT=gs://scraping-result-warehouse
DATE=`date '+%Y_%m_%d_%H%M%S'`

# gsutil cp 
gsutil cp $GCS_SCRAPY_ROOT/recruitments/recruitments_bk_next_*.sql /app/database/recruitments_next.sql
gsutil cp $GCS_SCRAPY_ROOT/recruitments/recruitments_bk_rag_*.sql /app/database/recruitments_rag.sql
gsutil cp $GCS_SCRAPY_ROOT/recruitments/recruitments_bk_workport_*.sql /app/database/recruitments_workport.sql

echo "1.Fin gsutil cp"
# check which table is cold stand by.
echo "SELECT id FROM journal.switching_routes WHERE onProgress = 0;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db > ./cat.txt

NUM=`cat ./cat.txt`

echo "2.Fin check which table is cold stand by."
# replace tablename of sql
sed -i -E "s/recruitments_next/recruitments_$NUM/g" /app/database/recruitments_next.sql
sed -i -E "s/recruitments_rag/recruitments_$NUM/g" /app/database/recruitments_rag.sql
sed -i -E "s/recruitments_workport/recruitments_$NUM/g" /app/database/recruitments_workport.sql

echo "3.Fin replace tablename of sql by sed"
# truncate cold stand by table.
echo "TRUNCATE TABLE journal.recruitments_$NUM;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db

echo "4.Fin truncate cold stand by table."
# insert new records into cold stand by table.
mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h db journal < /app/database/recruitments_next.sql
mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h db journal < /app/database/recruitments_rag.sql
mysql -u journal -p'awE_M1Y@Xp>xUW(s' -h db journal < /app/database/recruitments_workport.sql

echo "5.Fin Mysql import"
# change cold / hot mode and backup the recent data.
if [ $NUM = 1 ] ; then
	echo "UPDATE journal.switching_routes SET onProgress=1 WHERE id = 1;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db
	echo "UPDATE journal.switching_routes SET onProgress=0 WHERE id = 2;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db

	mysqldump -u journal -p'awE_M1Y@Xp>xUW(s' -h db -t journal recruitments_2 > /app/gcs/build/recruitments_snap_2_$DATE.sql
	gsutil cp /app/gcs/build/recruitments_snap_2_$DATE.sql $GCS_SCRAPY_ROOT/recruitments/snap/recruitments_snap_2_$DATE.sql
	echo "TRUNCATE TABLE journal.recruitments_2;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db

elif [ $NUM = 2 ] ; then
	echo "UPDATE journal.switching_routes SET onProgress=0 WHERE id = 1;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db
	echo "UPDATE journal.switching_routes SET onProgress=1 WHERE id = 2;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db

	mysqldump -u journal -p'awE_M1Y@Xp>xUW(s' -h db -t journal recruitments_1 > /app/gcs/build/recruitments_snap_1_$DATE.sql
	gsutil cp /app/gcs/build/recruitments_snap_1_$DATE.sql $GCS_SCRAPY_ROOT/recruitments/snap/recruitments_snap_1_$DATE.sql
	echo "TRUNCATE TABLE journal.recruitments_1;" | mysql -u journal -ss -p'awE_M1Y@Xp>xUW(s' -h db
fi

echo "6.Fin change cold / hot mode and backup the recent data."
# remove unnecessary files
rm ./cat.txt
rm /app/database/recruitments_next.sql
rm /app/database/recruitments_rag.sql
rm /app/database/recruitments_workport.sql
rm /app/gcs/build/recruitments_snap_*.sql

echo "7.Fin remove unnecessary files."

# make sitemap
cd /app
sed -i -E 's/APP_ENV=production/APP_ENV=local/g' /app/.env
php artisan db:seed --class=SitemapsSeeder
sed -i -E 's/APP_ENV=local/APP_ENV=production/g' /app/.env

echo "8.Fin Making sitemaps."