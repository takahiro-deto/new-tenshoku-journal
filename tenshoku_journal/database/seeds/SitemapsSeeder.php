<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SitemapsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('sitemaps')->delete();

        $base_url = "http://tenshoku-journal.com";
        
        if(preg_match('/localhost/',getenv('APP_URL') == 'http://localhost'))
        {
        	$base_url = "http://localhost";
        }

        $sitemaps = [
	        [$base_url . '/',Carbon::now()->subDay(2),'1.0','Weekly'],
	        [$base_url . '/about',Carbon::now()->subMonth(1),'0.5', 'monthly'],
	        [$base_url . '/ranking',Carbon::now()->subMonth(1),'0.5', 'monthly'],
	        [$base_url . '/ranking/site',DB::table('rankings')->orderBy('updated_at','DESC')->first()->updated_at, '0.5', 'monthly'],
	        [$base_url . '/ranking/agent',DB::table('rankings')->orderBy('updated_at','DESC')->first()->updated_at, '0.5', 'monthly'],
        ];

        /**
		 * Ranking
		 */

        $rankings = DB::table('rankings')->whereNotNull('updated_at')->orderBy('service_type','id')->get();

        foreach($rankings as $ranking)
		{
			$relative_path = "/ranking/{$ranking->service_type}/{$ranking->service_id}";
			$absolute_path = $base_url . $relative_path;

			array_push($sitemaps, [$absolute_path,$ranking->updated_at,'0.8','monthly']);
		}


		/**
		 * Knowhow
		 */

		$post = DB::table('wp_posts')->where('post_type','post')->orderBy('post_modified','DESC')->first();
		array_push($sitemaps, [$base_url . '/knowhow',$post->post_modified, '1.0', 'weekly']);



		/**
		 * Recruitments
		 */

		$search_top_timestamp = DB::table('recruitments')->select('last_confirmed_at')
																->orderBy('last_confirmed_at')
																->first()->last_confirmed_at;

		array_push($sitemaps,[$base_url . '/search',$search_top_timestamp,'0.9', 'weekly']);

		$job_codes = DB::table('jobcodes')->select('pathname','job_code_big')->distinct()->get();
		
		foreach ($job_codes as $job_code)
		{
			$relative_path = "/search/job/{$job_code->pathname}";
			$absolute_path = $base_url . $relative_path;

			array_push($sitemaps,[$absolute_path,$search_top_timestamp,'0.9','weekly']);
		}
		
		$recruitments = DB::select(DB::raw("select distinct rqmt_id,last_confirmed_at,job_code_full,pathname from (select rqmt_id,last_confirmed_at,t1.job_code_full,pathname from recruitments as t1 left outer join jobcodes as t2 on t1.job_code_full = t2.job_code_full order by last_confirmed_at DESC limit 40000 ) as t"));
		
		 

		foreach ($recruitments as $recruitment)
		{
			$relative_path = "/search/job/{$recruitment->pathname}/{$recruitment->job_code_full}/{$recruitment->rqmt_id}";
			$absolute_path = $base_url . $relative_path;
			array_push($sitemaps,[$absolute_path,$recruitment->last_confirmed_at,'0.7','weekly']);
		}

		// for ($i = 0; $i < count($sitemaps); $i++)
		// {
		// 	DB::table('sitemaps')->insert([
		// 		'url'        => $sitemaps[$i][0],
		// 		'updated_at' => $sitemaps[$i][1],
		// 		'priority'   => $sitemaps[$i][2],
		// 		'freq'       => $sitemaps[$i][3],
		// 	]);			
		// }

		$xml_1 = <<<'EOS'
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
EOS;
		
		$xml_3 = <<<EOS
</urlset>
EOS;

		file_put_contents('/app/gcs/build/sitemap.xml',$xml_1,FILE_APPEND);
		
		foreach($sitemaps as $sitemap)
		{
			$url        = $sitemap[0];
			$updated_at = $sitemap[1];
			$updated_at = preg_replace('/\s*\d{2}:\d{2}:\d{2}/','',$updated_at);
			$priority   = $sitemap[2];
			$freq       = $sitemap[3];
			
			file_put_contents('/app/gcs/build/sitemap.xml',PHP_EOL,FILE_APPEND);
			file_put_contents('/app/gcs/build/sitemap.xml',<<<EOS
<url>
  <loc>$url</loc>
  <lastmod>$updated_at</lastmod>
  <changefreq>$freq</changefreq>
  <priority>$priority</priority>
</url>
EOS
			,FILE_APPEND);
		}

		file_put_contents('/app/gcs/build/sitemap.xml',PHP_EOL,FILE_APPEND);
		file_put_contents('/app/gcs/build/sitemap.xml',$xml_3,FILE_APPEND);

		rename('/app/gcs/build/sitemap.xml','/app/public/sitemap.xml');
    }
}
