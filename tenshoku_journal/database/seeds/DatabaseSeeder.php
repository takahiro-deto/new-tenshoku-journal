<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('SiteRankingsTableSeeder');
        $this->call('AgentRankingsTableSeeder');
        $this->call('ItwebRankingsTableSeeder');
        $this->call('HakenRankingsTableSeeder');
        $this->call('WomanRankingsTableSeeder');
        $this->call('ReputationsTableSeeder');
        $this->call('JobcodesTableSeeder');
        $this->call('AreacodesTableSeeder');
        $this->call('ServicesTableSeeder');
        $this->call('SwitchingRoutesTableSeeder');
        $this->call('SitemapsSeeder');
    }
}
