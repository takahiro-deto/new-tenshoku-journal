<?php

use Illuminate\Database\Seeder;

class SwitchingRoutesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('switching_routes')->delete();

        DB::table('switching_routes')->insert([
        	'id' => 1,
        	'onProgress' => 1
    	]);

    	DB::table('switching_routes')->insert([
        	'id' => 2,
        	'onProgress' => 0
    	]);
    }
}
