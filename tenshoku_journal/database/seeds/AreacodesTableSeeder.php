<?php

use Illuminate\Database\Seeder;
use App\Areacode;

class AreacodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('areacodes')->delete();

        $data = [
	        '1'  => ['北海道', 'hokkaido','01','hokkaido','北海道'],
			'2'  => ['青森', 'aomori','02','tohoku','東北'],
			'3'  => ['秋田', 'akita','02','tohoku','東北'],
			'4'  => ['岩手','iwate','02','tohoku','東北'],
			'5'  => ['山形', 'yamagata','02','tohoku','東北'],
			'6'  => ['宮城', 'miyagi','02','tohoku','東北'],
			'7'  => ['福島', 'fukushima','02','tohoku','東北'],
			'8'  => ['茨城','ibaraki','03','kanto','関東'],
			'9'  => ['栃木','tochigi','03','kanto','関東'],
			'10' => ['群馬', 'gunma','03','kanto','関東'],
			'11' => ['埼玉','saitama','03','kanto','関東'],
			'12' => ['東京','tokyo','03','kanto','関東'],
			'13' => ['神奈川','kanagawa','03','kanto','関東'],
			'14' => ['千葉','chiba','03','kanto','関東'],
			'15' => ['静岡','shizuoka','04','tokai','東海'],
			'16' => ['愛知','aichi','04','tokai','東海'],
			'17' => ['岐阜','gihu','04','tokai','東海'],
			'18' => ['三重','mie','04','tokai','東海'],
			'19' => ['山梨','yamanashi','05','hokushinetsu','北信越'],
			'20' => ['長野','nagano','05','hokushinetsu','北信越'],
			'21' => ['新潟','niigata','05','hokushinetsu','北信越'],
			'22' => ['富山','toyama','05','hokushinetsu','北信越'],
			'23' => ['石川','ishikawa','05','hokushinetsu','北信越'],
			'24' => ['福井','fukui','05','hokushinetsu','北信越'],
			'25' => ['大阪','osaka','06','kansai','関西'],
			'26' => ['京都','kyoto','06','kansai','関西'],
			'27' => ['滋賀','shiga','06','kansai','関西'],
			'28' => ['奈良','nara','06','kansai','関西'],
			'29' => ['和歌山','wakayama','06','kansai','関西'],
			'30' => ['兵庫','hyogo','06','kansai','関西'],
			'31' => ['岡山','okayama','07','chugokuShikoku','中国・四国'],
			'32' => ['鳥取','tottori','07','chugokuShikoku','中国・四国'],
			'33' => ['島根','shimane','07','chugokuShikoku','中国・四国'],
			'34' => ['広島','hiroshima','07','chugokuShikoku','中国・四国'],
			'35' => ['山口','yamaguchi','07','chugokuShikoku','中国・四国'],
			'36' => ['香川','kagawa','07','chugokuShikoku','中国・四国'],
			'37' => ['愛媛','ehime','07','chugokuShikoku','中国・四国'],
			'38' => ['徳島','tokushima','07','chugokuShikoku','中国・四国'],
			'39' => ['高知','kochi','07','chugokuShikoku','中国・四国'],
			'40' => ['福岡','fukuoka','08','kyushu','九州・沖縄'],
			'41' => ['大分','oita','08','kyushu','九州・沖縄'],
			'42' => ['佐賀','saga','08','kyushu','九州・沖縄'],
			'43' => ['長崎','nagasaki','08','kyushu','九州・沖縄'],
			'44' => ['宮崎','miyazaki','08','kyushu','九州・沖縄'],
			'45' => ['熊本','kumamoto','08','kyushu','九州・沖縄'],
			'46' => ['鹿児島','kagoshima','08','kyushu','九州・沖縄'],
			'47' => ['沖縄','okinawa','08','kyushu','九州・沖縄'],
        ];

        foreach($data as $key => $value){
			Areacode::create([
				'area_code'        => $key,
				'area_code_value'  => $value[0],
				'area_pathname'    => $value[1],
				'block_code'       => $value[2],
				'block_code_value' => $value[4],
				'block_pathname'   => $value[3],
			]);
        }
    }
}
