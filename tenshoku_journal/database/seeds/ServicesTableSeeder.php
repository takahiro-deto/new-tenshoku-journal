<?php

use Illuminate\Database\Seeder;
use App\Service;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->delete();

        $data = [
			# site
			// VC
			['rikunabi_next','リクナビNEXT','site',1,1,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593127" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593127" height="1" width="1" border="0">いますぐ登録！</a>'],
			['baitoru_next','バイトルNEXT','site',1,0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593146" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593146" height="1" width="1" border="0">いますぐ登録！</a>'],
			['hatalike','はたらいく','site',1,0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593144" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593144" height="1" width="1" border="0">いますぐ登録！</a>'],
			// ['at_type','@type','site',1,0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593131" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593131" height="1" width="1" border="0">いますぐ登録！</a>'],
			['at_type','@type','site',1,0,'<a href="https://type.jp/" target="_blank" rel="nofollow">いますぐ登録！</a>'],

			# woman
			// VC
			['torabayu','とらばーゆ','woman',1,0,"https://toranet.jp"],
			['woman_type', '女の転職＠type', 'woman', 1, 0,"https://woman-type.jp/"],
			// A8
			['libz_career', 'LiBzCAREER', 'woman', 1, 0,"https://libinc.jp/"],

			# itweb
			// VC
			['geekly', 'GEEKLY', 'itweb', 1, 0,"http://example.com"],
			['workport_it', 'ワークポート', 'itweb', 1, 1,"http://example.com"],
			['pasona_tech', 'パソナテック', 'itweb', 1, 0,"http://example.com"],
			// A8
			['findjob', 'Find Job!', 'itweb', 1, 0,"http://example.com"],
			['mynabi_creator', 'マイナビクリエイター', 'itweb', 1, 0,"http://example.com"],
			['switch', 'Switch', 'itweb', 1, 0,"http://example.com"],
			['levatech_career','レバテックキャリア', 'itweb',1,0,"http://example.com"],

			# agent
			// VC
			['aidem_smart_agent', 'AIDEM SMART AGENT', 'agent', 1, 0,"https://smartagent.jp/"],
			['recruit_agent', 'リクルートエージェント', 'agent', 1, 1,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593123" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593123" height="1" width="1" border="0">いますぐ登録！</a>'],
			// ['pasona_career', 'パソナキャリア', 'agent', 1, 0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593137" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593137" height="1" width="1" border="0">いますぐ登録！</a>'],
			['pasona_career', 'パソナキャリア', 'agent', 1, 0,'<a href="https://www.pasonacareer.jp/" target="_blank" rel="nofollow">いますぐ登録！</a>'],
			// ['type_agent', 'type転職エージェント', 'agent', 1, 0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593134" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593134" height="1" width="1" border="0">いますぐ登録！</a>'],
			['type_agent', 'type転職エージェント', 'agent', 1, 0,'<a href="https://type.career-agent.jp/" target="_blank" rel="nofollow">いますぐ登録！</a>'],			
			['workport', 'ワークポート', 'agent', 1, 1,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593141" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593141" height="1" width="1" border="0">いますぐ登録！</a>'],
			['mynabi_agent', 'マイナビエージェント', 'agent', 1, 0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593136" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593136" height="1" width="1" border="0">いますぐ登録！</a>'],
			// ['jac_recruitment', 'JAC Recruitment', 'agent', 1, 0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593135" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593135" height="1" width="1" border="0">いますぐ登録！</a>'],
			['jac_recruitment', 'JAC Recruitment', 'agent', 1, 0,'<a href="http://www.jac-recruitment.jp/" target="_blank" rel="nofollow">いますぐ登録！</a>'],
			['doda', 'DODA', 'agent', 1, 0,'<a href="//ck.jp.ap.valuecommerce.com/servlet/referral?sid=3304402&pid=885593128" target="_blank" rel="nofollow"><img src="//ad.jp.ap.valuecommerce.com/servlet/gifbanner?sid=3304402&pid=885593128" height="1" width="1" border="0">いますぐ登録！</a>'],

			# haken
			// VC
			['recruit_staffing','リクルートスタッフィング', 'haken',1,0,"http://example.com"],
			['rikunabi_haken','リクナビ派遣', 'haken',1,0,"http://example.com"],
			// A8
			['mynabi_staff','マイナビスタッフ', 'haken',1,0,"http://example.com"],
			['hatalactive','ハタラクティブ派遣', 'haken',1,0,"http://example.com"],
			['temp_staff','テンプスタッフ', 'haken',1,0,"http://example.com"],
        ];

        foreach($data as $d)
        {
            Service::create([
				'service_id'      => $d[0],
				'service_jp_name' => $d[1],
				'belongs_to'      => $d[2],
				'publishing_flag' => $d[3],
				'crawling_flag'   => $d[4],
				'url'             => $d[5],
            ]);
        }
    }
}
