<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRankingMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ranking_metas', function (Blueprint $table) {
            $table->unsignedInteger('meta_id')->index();
            
            $table->string('positive_point_1',255);
            $table->string('positive_point_2',255)->nullable();
            $table->string('positive_point_3',255)->nullable();
            $table->string('negative_point_1',255);
            $table->string('negative_point_2',255)->nullable();
            $table->string('negative_point_3',255)->nullable();
            $table->text('description_1');
            $table->text('description_2');
            $table->text('description_3');
            $table->text('description_4');
            $table->text('description_5')->nullable();
            $table->text('embeded_1')->nullable();
            $table->text('embeded_2')->nullable();
            $table->text('embeded_3')->nullable();
            $table->text('embeded_4')->nullable();
            $table->text('embeded_5')->nullable();

            $table->timestamps();

            $table->foreign('meta_id')->references('id')->on('rankings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ranking_metas');
    }
}
