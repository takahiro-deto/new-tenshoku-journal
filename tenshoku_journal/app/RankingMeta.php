<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RankingMeta extends Model
{
    public function ranking()
    {
    	return $this->belongsTo('App\Ranking','id','meta_id');
    }
}
