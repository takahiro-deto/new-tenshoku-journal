<?php

use DB;

function dynamicSubstring($string)
{
	if(mb_strlen($string) >= 15)
	{
		$string = mb_substr($string, 0, 14);
		$string .= '...';
	}

	return $string;
}

function make_relative_path($path)
{
	return preg_replace('/http(s)?:\/\/([\w-]+\.)+[\w-]+/','',$path); 
}

function current_date()
{
	return strftime('%m月%d日',time());
}

function get_service_af_link($service_id,$original_url)
{
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$m = preg_match('/Googlebot/',$ua);
	$original_url = "<a class='c-searchCassette__link' href='${original_url}'>いますぐ登録！</a>";
	$url = $original_url;

	if(! $m)
	{		
		$url = DB::table('services')->where('service_id',$service_id)->first()->url;
		$url = preg_replace('/<a href/',"<a class='c-searchCassette__link' href",$url);	
	}

    return $url;
}

function get_service_af_link_at_show_page($service_id,$service_jp_name,$original_url)
{
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$m = preg_match('/Googlebot/',$ua);
	$original_url = "<a class='ui orange button c-single__button' href='${original_url}'>${service_jp_name}に登録</a>";
	$url = $original_url;

	if(! $m)
	{		
		$url = DB::table('services')->where('service_id',$service_id)->first()->url;
		$url = preg_replace('/<a href/',"<a class='ui orange button c-single__button' href",$url);
		$url = preg_replace('/いますぐ/',"${service_jp_name}に",$url);		
	}

    return $url;
}

function get_area_pathname($area_code)
{	
	$area_code = json_decode($area_code)->code;
	return DB::table('areacodes')->where('area_code',$area_code)->first()->area_pathname;
}

function get_canonical_url($url,$without_params=true)
{
	$q = null;
	
	if(! $without_params)
	{
		$q = $_SERVER['QUERY_STRING'];	
	}	
	
	if($q)
	{
		$url = $url . '?' . $q;
	}

	return $url;
}

