<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ranking;
use App\RankingMeta;
use App\Reputation;
use App\Recruitment;
use DB;

class RankingController extends Controller
{
    public function index($service_type = null)
    {        
        $headers = [
            'metaKeywords' => "",
            'metaDescription' => "",
            'title' => ""
        ];
        
        $service_type = is_null($service_type) ? 'top' : $service_type;
        $template = $service_type == 'top' ? 'ranking.top' : 'ranking.index';
        
        $vars = $this->makeVars($service_type);

        $headers['metaKeywords']    = $vars['metaKeywords'];
        $headers['metaDescription'] = $vars['metaDescription'];
        $headers['title']           = $vars['title'];
        
        return view($template,compact('headers','vars'));
    }

    public function show($service_type,$service_id)
    {
        $table_id = DB::table('switching_routes')->select('id')->where('onProgress',1)->first()->id;

        $headers = [
            'metaKeywords' => "",
            'metaDescription' => "",
            'title' => ""
        ];


        $vars = [];

        $vars['pages']       = $this->getPages();
        $vars['posts']       = $this->getPosts();

        $id                  = Ranking::where('service_type',$service_type)
                                ->where('service_id',$service_id)
                                ->first()->id;                
                
        $vars['contents']    = DB::table('rankings')
                                ->join('ranking_metas','rankings.id','ranking_metas.meta_id')
                                ->join('services','rankings.service_id','services.service_id')
                                ->where('rankings.id',$id)
                                ->first();

        $vars['good_reps']   = Ranking::find($id)
                                ->reputations()
                                ->orderBy('rating','desc')
                                ->orderBy('virtual_created_date','desc')
                                ->take(3)->get();
        $vars['bad_reps']    = Ranking::find($id)
                                ->reputations()
                                ->orderBy('rating','asc')
                                ->orderBy('virtual_created_date','desc')
                                ->take(3)->get();
        $vars['recent_reps'] = Ranking::find($id)
                                ->reputations()
                                ->orderBy('rating','desc')
                                ->orderBy('virtual_created_date','desc')
                                ->take(10)->get();

        $vars['job_postings_gross_cnt'] = DB::table("recruitments_${table_id}")->where('sitename',$service_id)->count();

        $vars['job_postings'] = DB::select(DB::Raw("select t.job_code_big_value,count(rqmt_id) as cnt from (select t2.job_code_big_value,rqmt_id from recruitments_${table_id} as t1 inner join jobcodes as t2 on t1.job_code_full = t2.job_code_full where t1.sitename = '$service_id') as t  group by job_code_big_value"));


        $headers['metaKeywords']    = "転職、求人、転職サイト、転職エージェント、口コミ、評判";
        $headers['metaDescription'] = preg_replace('/( |\t|\n)/',"",strip_tags($vars['contents']->summary));
        $headers['title']           = "{$vars['contents']->service_jp_name}の評判・口コミ";        

        return view('ranking.show',compact('headers','vars'));
    }


    private function makeVars($service_type)
    {
        $vars  = [];

        /**
         * for wordpress
         */
        $vars['pages'] = $this->getPages();
        $vars['posts'] = $this->getPosts();

        /**
         * for main contents
         */
        switch ($service_type)
        {
            case 'top':

                $types = ['site','agent','itweb','woman'];
                foreach($types as $type)
                {
                    $vars[$type] = DB::table('rankings as t1')
                                    ->leftJoin('ranking_metas as t2','t1.id','=','t2.meta_id')
                                    ->where('service_type',$type)
                                    ->orderBy('rank')
                                    ->get();
                }

                $vars['metaKeywords']    = "転職、求人、転職サイト、転職エージェント、口コミ、評判";
                $vars['metaDescription'] = "転職の理由は人によって様々です。スキルを磨いて今の仕事でキャリアアップしていきたい人もいれば、未経験で全く新しい仕事にチャレンジしてみたい人もいます。またポジティブな理由だけじゃなく、今の仕事がとにかく嫌で辞めたいけれどやりたいことがないといった人もいれば、育児や介護など家族の都合で働きたいけど選択肢が限られる人もいます。転職ジャーナルでは単に人気のサイトをオススメするのではなく、タイプに合わせて利用すべきサービスを紹介しています。";
                $vars['title']           = "転職サイト・エージェント徹底比較ランキング";
                
                break;

            case 'site':

                $vars['service_type']   = 'site';
                $vars['h1Text']         = '結局どのサイトがいいの？みんなの口コミから選ぶ転職サイト総合ランキング';
                $vars['breadcrumbText'] = '転職サイト総合ランキング';
                $vars['headline']       = '「求人は自分の目でしっかりあう求人を探したい」「エージェントから提案されたものだけだと見逃している求人がある気がする」など 転職活動に時間がしっかりと割け、なおかつ可能性を狭めたくないなら、提案型の転職エージェントよりも転職サイトの利用がオススメです。 複数の転職サイトの中から、利用者数・求人数・機能など総合的に見て、オススメの転職サイトを集めました';

                $vars['metaKeywords']    = "転職、求人、転職サイト、転職エージェント、口コミ、評判";
                $vars['metaDescription'] = '「求人は自分の目でしっかりあう求人を探したい」「エージェントから提案されたものだけだと見逃している求人がある気がする」など 転職活動に時間がしっかりと割け、なおかつ可能性を狭めたくないなら、提案型の転職エージェントよりも転職サイトの利用がオススメです。 複数の転職サイトの中から、利用者数・求人数・機能など総合的に見て、オススメの転職サイトを集めました';
                $vars['title']           = "みんなの口コミから選ぶ転職サイト総合ランキング";
                                
                break;

            case 'agent':
                
                $vars['service_type']   = 'agent';
                $vars['h1Text']         = '評判・口コミで選ぶ！転職エージェント徹底比較ランキング';
                $vars['breadcrumbText'] = '評判・口コミで選ぶ！転職エージェント徹底比較ランキング';
                $vars['headline']       = '転職エージェントなんてスキルの高い人が利用するものでしょ？なんて思っていませんか。
                        実は利用者だけでいえば、若手や第二新卒の人が最も多いのです。転職エージェントを利用すべきメリットについては詳しくはこちらの記事を見ていただければと思いますが、端的にいってしまえば、

                        ・ 転職サイトには掲載されない数万件の非公開求人の取り扱い
                        ・ 面接、職務経歴書の対策・サポートが充実
                        ・ 年収交渉、面接日程調整、内定後の手続き、退職交渉のフォローなどのサポート

                        以上３点が転職サイトの利用との大きな違いです。
                        「職務経歴書の書き方がわからない」「面接が不安」などお悩みの方は、まずは気軽にエージェントとの面談をしていただくことをオススメします。以下に主要な転職エージェントをランキング形式でまとめました。';

                $vars['metaKeywords']    = "転職、求人、転職サイト、転職エージェント、口コミ、評判";
                $vars['metaDescription'] = '転職エージェントなんてスキルの高い人が利用するものでしょ？なんて思っていませんか。実は利用者だけでいえば、若手や第二新卒の人が最も多いのです。転職エージェントには転職サイトと違い、「非公開求人の取り扱い」「面接、職務経歴書の対策・サポートが充実」「年収交渉、面接日程調整、内定後の手続き、退職交渉のフォローなどのサポート」と言ったメリットがあります。「職務経歴書の書き方がわからない」「面接が不安」などお悩みの方は、まずは気軽にエージェントとの面談をしていただくことをオススメします。以下に主要な転職エージェントをランキング形式でまとめました。';
                $vars['title']           = "評判・口コミで選ぶ！転職エージェント徹底比較ランキング";

                break;

            case 'itweb':
                
                $vars['service_type']   = 'itweb';
                $vars['h1Text']         = 'ITWeb系転職サイト・エージェントランキング';
                $vars['breadcrumbText'] = 'ITWeb系転職サイト・エージェントランキング';
                $vars['headline']       = 'ここ数年の間に、IT・Web業界を専門とする転職エージェントや転職サイトが一気に増えてきています。
                    スタートアップやベンチャー系の企業を目指すような方の場合、必ずしも大手総合転職エージェントの利用がベストとは限りません。
                    ここでは、新興ながら近年で一気に実績を増やしてきている転職サイト・転職エージェントをまとめました。';

                break;

            case 'woman':

                $vars['service_type']   = 'woman';
                $vars['h1Text']         = '女性向け転職サイト・転職エージェント総合ランキング';
                $vars['breadcrumbText'] = '女性向け転職サイト・転職エージェント総合ランキング';
                $vars['headline']       = '働き方の多様性や女性の社会進出が叫ばれる中、どうやって仕事を続けていけばいいのか不安になることもありますよね。バリバリ仕事を続けていきたいキャリアタイプの方、 家庭と仕事を両立したいタイプの方にも合う女性向け転職サービスランキングです。';       

                break;

            case 'haken':

                $vars['service_type']   = 'haken';
                $vars['h1Text']         = '登録はここで決まり！派遣サイトおすすめランキング';
                $vars['breadcrumbText'] = '登録はここで決まり！派遣サイトおすすめランキング';
                $vars['headline']       = '';

                break;

            default:
                
                break;
        }

        if($service_type !== 'top')
        {
            $vars['rankings'] = DB::table('rankings as t1')
                                ->leftJoin('ranking_metas as t2','t1.id','=','t2.meta_id')
                                ->join('services as t3','t1.service_id','=','t3.service_id')
                                ->where('t3.publishing_flag',1)
                                ->where('t1.service_type',$service_type)
                                ->orderBy('t1.rank','asc')
                                ->get();   

            $vars['reputations'] = $this->getRelatedReputation($service_type);
        }


        return $vars;
    }    

    private function getRelatedReputation($service_type)
    {
        /**
         * ranking eloquentと同じ順序でsortしたreputations collectionを取得
         */

        $rankings = array_flatten(Ranking::where('service_type',$service_type)
                    ->select('id')
                    ->orderBy('rank','asc')
                    ->get()
                    ->toArray());

        $reps = [];

        foreach($rankings as $rank)
        {
            $reps[] = Ranking::find($rank)->reputations()
                                        ->orderBy('rating','desc')
                                        ->take(2)
                                        ->get();
        }

        $reps = collect($reps);


        return $reps;
    }

    private function getPages()
    {
    	$pages = DB::table('wp_posts as t1')
    				->join('wp_postmeta as t2','t2.post_id','=','t1.ID')
    				->select('ID','post_title','post_name','guid','meta_value as headline')
    				->distinct()
    				->where('t1.post_type','=','page')
    				->where('t2.meta_key','=','headline')
    				->get();

		return $pages;    	
    }

    private function getPosts()
    {
    	$posts = DB::table('wp_posts as t1')
    				->leftJoin('wp_posts as t2','t1.ID','=','t2.post_parent')
    				->select('t1.post_title as title','t1.guid as link','t2.guid as thumb')
    				->where('t1.post_type','=','post')
    				->where('t2.guid','REGEXP','wp-content/uploads/[0-9]{4}/[0-9]{2}')
    				->orderBy('t1.post_modified','DESC')
    				->take(6)
    				->get();

    	return $posts;
    }
}
