<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recruitment;
use DB;

class TopController extends Controller
{
    public function index()
    {
        /**
         * headers
         */
        $headers = [
            'metaKeywords' => "",
            'metaDescription' => "",
            'title' => ""];
        
        $headers['metaKeywords'] = "転職、求人、転職サイト、転職エージェント、口コミ、評判";
        $headers['metaDescription'] = "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。";
        $headers['title'] = "転職を考える全ての人のためのサイト";


        /**
         * for wordpress
         */
    	$pages = DB::table('wp_posts as t1')
    				->join('wp_postmeta as t2','t2.post_id','=','t1.ID')
    				->select('ID','post_title','post_name','guid','meta_value as headline')
    				->distinct()
    				->where('t1.post_type','=','page')
    				->where('t2.meta_key','=','headline')
    				->get();

    	$posts = DB::table('wp_posts as t1')
    				->leftJoin('wp_posts as t2','t1.ID','=','t2.post_parent')
    				->select('t1.post_title as title','t1.guid as link','t2.guid as thumb')
    				->where('t1.post_type','=','post')
    				->where('t2.guid','REGEXP','wp-content/uploads/[0-9]{4}/[0-9]{2}/')
    				->orderBy('t1.post_modified','DESC')
    				->take(10)
    				->get();


        /**
         * for main contents
         */
    	$vars = [
    		'results' => "",
    		'counts'  => 0,
    		'last_update' => ""
    	];

        $id = DB::table('switching_routes')->select('id')->where('onProgress',1)->first()->id;

        $vars['results']     = DB::select(DB::raw("select distinct
                                                        *
                                                    from
                                                        (select
                                                            t1.sitename,
                                                            t1.rqmt_id,
                                                            t1.cmpny_name,
                                                            t1.job_code_full,
                                                            t2.pathname
                                                        from
                                                            (select
                                                                sitename,
                                                                rqmt_id,
                                                                cmpny_name,
                                                                job_code_full
                                                            from
                                                                recruitments_${id}
                                                            where
                                                                sitename = 'rikunabi_next'
                                                            order by
                                                                last_confirmed_at DESC
                                                            limit
                                                                20) as t1
                                                        inner join
                                                            jobcodes as t2
                                                        on
                                                            t1.job_code_full = t2.job_code_full) as t
                                                    limit
                                                        10;"));

        $vars['counts']      = DB::table("recruitments_${id} as t1")
                                ->join('jobcodes as t2','t1.job_code_full','t2.job_code_full')
                                ->count();

        $vars['last_update'] = substr(
                                DB::table("recruitments_${id}")
                                ->select('last_confirmed_at')
                                ->distinct()
                                ->orderBy('last_confirmed_at','desc')
                                ->first()
                                ->last_confirmed_at, 0, 10);


    	return view('page.index',compact('headers','pages','posts','vars'));
    }

    public function about()
    {
        /**
         * headers
         */
        $headers = [
            'metaKeywords' => "",
            'metaDescription' => "",
            'title' => ""];
        
        $headers['metaKeywords'] = "転職、求人、転職サイト、転職エージェント、口コミ、評判";
        $headers['metaDescription'] = "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。";
        $headers['title'] = "転職ジャーナルについて";

        return view('page.about',compact('headers'));
    }
}
