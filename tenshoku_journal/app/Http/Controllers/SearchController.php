<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobcode;
use App\Areacode;
use App\Recruitment;
use App\Ranking;
use DB;

class SearchController extends Controller
{
	public function index()
	{
        $id = $this->getDynamicRecruitmentTable();
        $currentUrl = \Request::fullUrl();
        $canonical_path = null;

        if(preg_match('/\?page=1/',$currentUrl))
        {
            $canonical_path = preg_replace('/\?page=1/', '', $currentUrl);
        }
        
        $headers = [
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。",
            'title' => "全国・全職種の求人一覧"
        ];
        if(isset($canonical_path))
        {
            $headers['canonical_path'] = $canonical_path;            
        }

		$data = $this->makeData();
		$cassettes = DB::table('jobcodes as t2')
						->join("recruitments_${id} as t1",'t1.job_code_full','t2.job_code_full')
                        ->whereIn('t2.job_code_full',function($query){
                            $query->select('job_code_full')
                                    ->distinct()
                                    ->from('jobcodes')
                                    ->get();
                        })->whereNotNull('t2.job_code_full')
                        ->orderBy('t1.last_confirmed_at','desc')                        
						->paginate(50);

		return view('search.index',compact('headers','cassettes','data'));
	}

    public function getIndexByJobBigCode($pathname)
    {
        $id = $this->getDynamicRecruitmentTable();

        $opt = ['pathname' => $pathname];

        $data = $this->makeData('jobcode',$opt);

        $currentUrl = \Request::fullUrl();
        $canonical_path = null;

        if(preg_match('/\?page=1/',$currentUrl))
        {
            $canonical_path = preg_replace('/\?page=1/', '', $currentUrl);
        }

        $headers = [
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。",
            'title' => "{$data['job_code_big_value']}の求人検索結果"
        ];

        if(isset($canonical_path))
        {
            $headers['canonical_path'] = $canonical_path;
        }

        $cassettes = DB::table("recruitments_${id} as t1")
                        ->join('jobcodes as t2','t1.job_code_full','t2.job_code_full')
                        ->whereIn('t1.job_code_full',$data['selected_job_code_list'])
                        ->whereIn('t2.job_code_full',function($query){
                            $query->select('job_code_full')
                                    ->distinct()
                                    ->from('jobcodes')
                                    ->get();
                        })->orderBy('t1.last_confirmed_at','desc')
                        ->paginate(50);

        return view('search.getIndexByJobBigCode',compact('headers','cassettes','data'));
    }

    public function getIndexByJobFullCode($pathname, $job_code_full)
    {
        $id = $this->getDynamicRecruitmentTable();

        $opt = ['pathname' => $pathname, 'job_code_full' => $job_code_full];

        $data = $this->makeData('jobcode',$opt);

        $currentUrl = \Request::fullUrl();
        $canonical_path = null;

        if(preg_match('/\?page=1/',$currentUrl))
        {
            $canonical_path = preg_replace('/\?page=1/', '', $currentUrl);
        }

        $headers = [
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。",
            'title' => "{$data['job_code_mid_value']}の求人検索結果"
        ];

        if(isset($canonical_path))
        {
            $headers['canonical_path'] = $canonical_path;
        }

        $cassettes = DB::table("recruitments_${id} as t1")
                        ->join('jobcodes as t2','t1.job_code_full','t2.job_code_full')
                        ->where('t1.job_code_full',$job_code_full)
                        ->whereIn('t2.job_code_full',function($query){
                            $query->select('job_code_full')
                                    ->distinct()
                                    ->from('jobcodes')
                                    ->get();
                        })->orderBy('last_confirmed_at','desc')
                        ->paginate(50);

        return view('search.getIndexByJobFullCode',compact('headers','cassettes','data'));
    }

    public function getIndexByBlockCode($block_pathname)
    {
        $id = $this->getDynamicRecruitmentTable();

        $opt = ['block_pathname' => $block_pathname];
        $area_code = Areacode::where('block_pathname',$block_pathname)->get();
        $values = [];
        foreach($area_code as $area)
        {
            array_push($values,$area->area_code);
        }        

        $data = $this->makeData('areacode',$opt);

        $currentUrl = \Request::fullUrl();
        $canonical_path = null;

        if(preg_match('/\?page=1/',$currentUrl))
        {
            $canonical_path = preg_replace('/\?page=1/', '', $currentUrl);
        }

        $headers = [
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。",
            'title' => "{$data['block_code_value']}の求人検索結果"
        ];

        if(isset($canonical_path))
        {
            $headers['canonical_path'] = $canonical_path;
        }

        $query = DB::table("recruitments_${id} as t1")->join('jobcodes as t2','t1.job_code_full','t2.job_code_full')  ;                

        for($i = 0; $i < count($values); $i++)
        {
            if($i == 0)
            {
                $v = json_encode($values[0]);
                $query->whereRaw("JSON_CONTAINS(t1.area_code,'${v}',\"$.code\")");
            }else{
                $v = json_encode($values[$i]);
                $query->orWhereRaw("JSON_CONTAINS(t1.area_code,'${v}',\"$.code\")");
            }            
        }
        $cassettes = $query->whereIn('t2.job_code_full',function($query){
                            $query->select('job_code_full')
                                    ->distinct()
                                    ->from('jobcodes')
                                    ->get();
                        })->orderBy('last_confirmed_at','desc')->paginate(50);

        $area_pathname = Areacode::where('block_pathname',$block_pathname)->first()->area_pathname;

        return view('search.getIndexByBlockCode',compact('headers','cassettes','data','area_pathname'));
    }

    public function getIndexByAreaCode($block_pathname, $area_pathname)
    {
        $id = $this->getDynamicRecruitmentTable();

        $opt = ['block_pathname' => $block_pathname, 'area_pathname' => $area_pathname];        
        $data = $this->makeData('areacode',$opt);

        $currentUrl = \Request::fullUrl();
        $canonical_path = null;

        if(preg_match('/\?page=1/',$currentUrl))
        {
            $canonical_path = preg_replace('/\?page=1/', '', $currentUrl);
        }

        $headers = [            
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。",
            'title' => "{$data['area_code_value']}の求人検索結果"
        ];

        if(isset($canonical_path))
        {
            $headers['canonical_path'] = $canonical_path;
        }

        # 北海道カノニカル対応
        if($area_pathname == 'hokkaido')
        {
            $headers['canonical_path'] = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . "{$_SERVER['HTTP_HOST']}/search/area/hokkaido";
            $headers['without_params'] = false;
        }


        $area_code = Areacode::where('area_pathname',$area_pathname)->first()->area_code;
        $v = json_encode(strval($area_code));

        $cassettes = DB::table("recruitments_${id} as t1")
                        ->join('jobcodes as t2','t1.job_code_full','t2.job_code_full')
                        ->whereRaw("JSON_CONTAINS(t1.area_code,'${v}',\"$.code\")")
                        ->whereIn('t2.job_code_full',function($query){
                            $query->select('job_code_full')
                                    ->distinct()
                                    ->from('jobcodes')
                                    ->get();
                        })->orderBy('last_confirmed_at','desc')
                        ->paginate(50);


        return view('search.getIndexByAreaCode',compact('headers','cassettes','data'));
    }

    public function getIndexByServiceId($service_id)
    {
        $id = $this->getDynamicRecruitmentTable();

        $headers = [
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。",
            'title' => ""
        ];

    	$opt = ['service_id' => $service_id];

        $data = $this->makeData('service',$opt);

        $currentUrl = \Request::fullUrl();
        $canonical_path = null;

        if(preg_match('/\?page=1/',$currentUrl))
        {
            $canonical_path = preg_replace('/\?page=1/', '', $currentUrl);
        }

        if(isset($canonical_path))
        {
            $headers['canonical_path'] = $canonical_path;
        }

        $cassettes = DB::table("recruitments_${id} as t1")
                        ->join('jobcodes as t2','t1.job_code_full','t2.job_code_full')
                        ->where('sitename',$service_id)
                        ->whereIn('t2.job_code_full',function($query){
                            $query->select('job_code_full')
                                    ->distinct()
                                    ->from('jobcodes')
                                    ->get();
                        })->orderBy('last_confirmed_at','desc')                        
                        ->paginate(50);

        return view('search.getIndexByServiceId',compact('headers','cassettes','data'));
    }

    public function showByJobCode($pathname, $job_code_full, $rqmt_id)
    {
        // $description = Recruitment::where('rqmt_id', $rqmt_id)->first();
        $id = $this->getDynamicRecruitmentTable();
        $description = DB::table("recruitments_${id}")->where('rqmt_id', $rqmt_id)->first();
        if(! $description)
        {
            abort('404');
        }

        $data = [
            'pathname'           => $pathname,
            'job_code_big_value' => Jobcode::wherePathname($pathname)->first()->job_code_big_value,
            'job_code_mid_value' => Jobcode::where('job_code_full',$job_code_full)->first()->job_code_mid_value,
            'job_code_full'      => $job_code_full,
            // 'relatedJob'         => Recruitment::where('job_code_full',$job_code_full)
            'relatedJob'         => DB::table("recruitments_${id}")
                                    ->where('job_code_full',$job_code_full)
                                    ->where('sitename',$description->sitename)
                                    ->orderBy('last_confirmed_at','desc')
                                    ->take(4)
                                   ->get(),
            'rankingContent'     => DB::table('rankings as t1')
                                    ->join('ranking_metas as t2','t1.id','t2.meta_id')
                                    ->where('service_id',$description->sitename)
                                    ->first(),            
        ];


        $headers = [
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "{$description->cmpny_name}の求人情報／{$data['job_code_mid_value']}の求人情報／{$data['rankingContent']->service_jp_name}の求人情報なら転職ジャーナル",
            'title' => "{$description->cmpny_name}の求人情報／{$data['job_code_mid_value']}の求人情報／{$data['rankingContent']->service_jp_name}の求人情報なら転職ジャーナル"
        ];
        

        return view('search.showByJobCode',compact('headers','description','data'));
    }

    public function showByAreaCode(Request $request, $block_pathname, $area_pathname, $rqmt_id)
    {
        $id = $this->getDynamicRecruitmentTable();
        // $description = Recruitment::where('rqmt_id', $rqmt_id)->first();
        $description = DB::table("recruitments_${id}")->where('rqmt_id', $rqmt_id)->first();
        if(! $description)
        {
            abort('404');
        }

        $job_code_obj = Jobcode::where('job_code_full',$description->job_code_full)->first();
        if(! $job_code_obj || ! $description)
        {
            abort('404');
        }
        $canonical_path = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . "{$_SERVER['HTTP_HOST']}/search/job/{$job_code_obj->pathname}/{$job_code_obj->job_code_full}/{$description->rqmt_id}";


        $data = [            
            'block_pathname'     => $block_pathname,
            'area_pathname'      => $area_pathname,
            'block_code_value'   => Areacode::where('block_pathname',$block_pathname)->first()->block_code_value,
            'area_code_value'    => Areacode::where('area_pathname', $area_pathname)->first()->area_code_value,
            // 'relatedJob'         => Recruitment::where('area_code', $description->area_code)
            'relatedJob'         => DB::table("recruitments_${id}")
                                    ->where('area_code', $description->area_code)
                                    ->where('sitename', $description->sitename)
                                    ->orderBy('last_confirmed_at','desc')
                                    ->take(4)
                                    ->get(),
            'rankingContent'     => DB::table('rankings as t1')
                                    ->join('ranking_metas as t2','t1.id','t2.meta_id')
                                    ->where('service_id',$description->sitename)
                                    ->first()
        ];


        $headers = [
            'canonical_path'     => $canonical_path,
            'without_params' => false,
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "{$description->cmpny_name}の求人情報／{$data['area_code_value']}の求人情報／{$data['rankingContent']->service_jp_name}の求人情報なら転職ジャーナル",
            'title' => "{$description->cmpny_name}の求人情報／{$data['area_code_value']}の求人情報／{$data['rankingContent']->service_jp_name}の求人情報なら転職ジャーナル"
        ];        


        return view('search.showByAreaCode',compact('headers','description','data'));
    }

    public function search(Request $request)
    {
        $id = $this->getDynamicRecruitmentTable();

        $headers = [
            'metaKeywords' => "転職、求人、転職サイト、転職エージェント、口コミ、評判",
            'metaDescription' => "転職ジャーナルは、転職を検討し始めたすべての人向けのライフスタイルマガジンです。自分にあった転職サイト・転職エージェントの選び方や、各転職サービスが掲載している求人を比較して、自分にあったサービスを見つけることができます。",
            'title' => "求人検索結果"
        ];        

        $_inputList         = $request->input();
        $_job_code_list     = [];
        $_area_code_list    = [];
        $_serviceList       = [];
        $_redirect          = false;
        $_keywordList       = [];


        if(!is_null($_inputList['keyword']))
        {
            $_keywordList       = preg_split('/,/',preg_replace('/(\s| )/',',', $_inputList['keyword']));
        }

        foreach($_inputList as $k => $v)
        {
            if(strpos($k,'ch_c_') !== false)
            {
                array_push($_job_code_list, strval(preg_replace('/ch_c_/', '', $k)));
            }
            if(strpos($k,'ch_a_') !== false)
            {
                array_push($_area_code_list, strval(preg_replace('/ch_a_/', '', $k)));
            }
            if(strpos($k,'s_') !== false)
            {
                array_push($_serviceList, strval(preg_replace('/s_/','', $k)));
            }            
        }

        if(count($_job_code_list) === 0 && count($_area_code_list) === 0 && count($_serviceList) === 0 && count($_keywordList) === 0)
        {
            return redirect()->action('SearchController@index');
        }


        $data = $this->makeData('custom',[
                    'selected_job_code_list'    => $_job_code_list,
                    'selected_area_code_list'   => $_area_code_list,
                    'selected_service_list'     => $_serviceList,
                ]);


        $_baseQuery = DB::table("recruitments_${id} as t1")
                        ->join('jobcodes as t2','t1.job_code_full','t2.job_code_full');

        if(count($_job_code_list) > 0)
        {
            $_baseQuery->whereIn('t1.job_code_full',$_job_code_list);
        }
        if(!empty($_keywordList))
        {
            $len = min(count($_keywordList),5);

            for($i = 0; $i < $len; $i++)
            {
                $_baseQuery->where('content','like',"%$_keywordList[$i]%");
            }
        }
        if(count($_area_code_list) > 0)
        {
            $_area_code_count = count($_area_code_list);            

            if($_area_code_count == 1){
                $v = json_encode(strval($_area_code_list[0]));
                $_baseQuery->whereRaw("JSON_CONTAINS(t1.area_code,'${v}',\"$.code\")");
            }else{
                for($i = 0; $i < $_area_code_count; $i++)
                {
                    $v = json_encode(strval($_area_code_list[$i]));
                    
                    if($i == 0){
                        $_baseQuery->whereRaw("JSON_CONTAINS(t1.area_code,'${v}',\"$.code\")");
                    }else{
                        $_baseQuery->orWhereRaw("JSON_CONTAINS(t1.area_code,'${v}',\"$.code\")");
                    }                    
                }
            }
            
        }
        if(count($_serviceList) > 0)
        {
            $_baseQuery->whereIn('t1.sitename',$_serviceList);
        }
        
        $cassettes = $_baseQuery->orderBy('t1.last_confirmed_at','desc')->paginate(50);

        return view('search.index',compact('headers','cassettes','data'));
    }

    private function makeData($type = null, $opt = null)
    {
    	$data = "";
    	$basedata = [
    		'service_id'             => '',
            'service_jp_name'        => '',
            'pathname'               => '',
            'block_pathname'         => '',
            'area_pathname'          => '',
            'job_code_big_value'     => '',
            'job_code_mid_value'     => '',
            'block_code_value'       => '',
            'area_code_value'        => '',
            'selected_job_code_list' => [],
            'selected_area_code_list'=> [],
            'selected_service_list'  => [],
            'job_modal'              => Jobcode::fulllist(),
            'area_modal'             => Areacode::fulllist(),
    	];

    	if(is_null($type) && is_null($opt))
        {
            $data = $basedata;

        }else if($type == 'jobcode'){

            if(isset($opt['pathname']) && isset($opt['job_code_full']))
            {
                $data = array_merge($basedata,[
                    'pathname'               => $opt['pathname'],
                    'job_code_big_value'     => Jobcode::wherePathname($opt['pathname'])->first()->job_code_big_value,
                    'job_code_mid_value'     => Jobcode::where('job_code_full',$opt['job_code_full'])->first()->job_code_mid_value,
                    'selected_job_code_list' => array($opt['job_code_full'])
                ]);

            }else if(isset($opt['pathname'])){

                $data = array_merge($basedata,[
                    'pathname' => $opt['pathname'],
                    'job_code_big_value' => Jobcode::wherePathname($opt['pathname'])->first()->job_code_big_value,
                ]);

                $_selected_job_code_list = [];
                foreach(Jobcode::select('job_code_full')->wherePathname($opt['pathname'])->get() as $v)
                {
                    array_push($_selected_job_code_list, $v->job_code_full);
                }
                $data['selected_job_code_list'] = $_selected_job_code_list;
            
            }else{
                $data = $basedata;
            }

        }else if($type == 'areacode'){

            if(isset($opt['block_pathname']) && isset($opt['area_pathname']))
            {
                $data = array_merge($basedata,[
                    'block_pathname'          => $opt['block_pathname'],
                    'area_pathname'           => $opt['area_pathname'],
                    'block_code_value'        => Areacode::where('block_pathname',$opt['block_pathname'])->first()->block_code_value,
                    'area_code_value'         => Areacode::where('area_pathname',$opt['area_pathname'])->first()->area_code_value,
                    'selected_area_code_list' => array(Areacode::where('area_pathname',$opt['area_pathname'])->first()->area_code),
                ]);

            }else if(isset($opt['block_pathname'])){

                $data = array_merge($basedata,[
                    'block_pathname' => $opt['block_pathname'],
                    'block_code_value' => Areacode::where('block_pathname',$opt['block_pathname'])->first()->block_code_value,
                ]);

                $_selected_area_code_list = [];
                foreach(Areacode::select('area_code')->where('block_pathname',$opt['block_pathname'])->get() as $v) 
                {
                    array_push($_selected_area_code_list, $v->area_code);
                }
                $data['selected_area_code_list'] = $_selected_area_code_list;
                
            }else{

                $data = $basedata;

            }

        }else if($type == 'service'){

            if(isset($opt['service_id']))
            {
                $data = array_merge($basedata,[
                    'service_id'            => $opt['service_id'],
                    'service_jp_name'       => DB::table('services')->where('service_id',$opt['service_id'])->first()->service_jp_name,
                    'selected_service_list' => $opt['service_id'],
                ]);
            }else{
                $data = $basedata;
            }
        }else if($type == 'custom'){

            if(isset($opt['selected_job_code_list']) && count($opt['selected_job_code_list']) > 0)
            {
                $data = array_merge($basedata,[
                    'selected_job_code_list' => $opt['selected_job_code_list']
                ]);
            }
            if(isset($opt['selected_area_code_list']) && count($opt['selected_area_code_list']) > 0)
            {
                $data = array_merge($basedata,[
                    'selected_area_code_list' => $opt['selected_area_code_list']
                ]);
            }
            if(isset($opt['selected_service_list']) && count($opt['selected_service_list']) > 0)
            {
                $data = array_merge($basedata,[
                    'selected_service_list' => $opt['selected_service_list']
                ]);
            }
            if(count($opt['selected_job_code_list']) == 0 && count($opt['selected_area_code_list']) == 0 && count($opt['selected_service_list']) == 0)
            {
                $data = $basedata;
            }
        }

        return $data;
    }

    private function getDynamicRecruitmentTable()
    {
        $id = DB::table('switching_routes')->select('id')->where('onProgress',1)->first()->id;

        return $id;
    }
}
