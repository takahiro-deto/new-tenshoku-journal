# new-tenshoku-journal

At first Install Docker on your Mac or Windows, linux. 
Then run below a few commands. 

```
$ mkdir app 
$ git clone https://github.com/TakahiroDee/new-tenshoku-journal.git 
$ docker-compose up
```

If you login app server, you should run next command.

```
$ docker-compose exec --user journal app bash
```

in app environment, you use journal user as usual
if you login by root, remove --user option.


## Configuration

In each Container, you should check host ip by `hostname -i`.

In App Container, you do have to below tasks. 
 
- make `.env` file and set DB container's ip to DB_HOST
- `composer install`
- `npm install`
- `npm run dev`
- change permission of `storage` directory and `bootstrap/cache` directroy recursively to 775, if those directories are not configured.
- `gpasswd -a apache journal`
 
 
In DB Container, you do have to below tasks. 
 
- RUN `CREATE USER journal IDENTIFIED WITH mysql_native_password BY ${password};`
- RUN `CREATE DATABASE journal DEFAULT CHARSET utf8mb4;`
- RUN `GRANT ALL PRIVILEGES ON journal.* TO 'journal'@${app_container's_ip};`
- RUN `FLUSH PRIVILEGES`
 
Then return to App Container, check whether you can access to DB Container by `mysql` commands and if OK,

- RUN `php artisan migrate`
- RUN `php artisan db:seed`




