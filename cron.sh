#!/usr/bin/bash
#PATH

# for GCP
PATH=/home/hello_gdesignlab2/.pyenv/shims:/home/hello_gdesignlab2/.pyenv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/hello_gdesignlab2/.local/bin:/home/hello_gdesignlab2/bin

cd /app
docker-compose exec -T app /bin/bash /app/gcs/build/exec.sh $1